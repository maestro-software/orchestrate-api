using System.Threading.Tasks;

namespace OrchestrateApi.Email
{
    public interface IEmailProvider
    {
        Task SendAsync(EmailMessage email);
    }
}