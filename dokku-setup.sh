#!/bin/bash

APP_NAME=orchestrate-api
DB_NAME=orchestrate-db
APP_PORT=5000
APP_DOMAIN=<your.tld>
EMAIL=<your@email.tld>

dokku apps:create $APP_NAME

sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git
dokku postgres:create $DB_NAME

dokku postgres:link $DB_NAME $APP_NAME

# Deploy using the following from a build machine
# git remote add dokku dokku@<dokku-host.site>:$APP_NAME
# git push dokku master

# Reverse proxy via HTTP to container rather than using exposed port that dokku sets
# See http://dokku.viewdocs.io/dokku/networking/port-management/#applications-using-expose
dokku proxy:ports-add $APP_NAME http:80:$APP_PORT
dokku proxy:ports-remove $APP_NAME http:$APP_PORT:$APP_PORT

# Make sure you're using the correct domain set up to point to the dokku box before HTTPS
dokku domains:set $APP_NAME $APP_DOMAIN

# HTTPS
sudo dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git
dokku config:set --global DOKKU_LETSENCRYPT_EMAIL=$EMAIL
dokku letsencrypt $APP_NAME