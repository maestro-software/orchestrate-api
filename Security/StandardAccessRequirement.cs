using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace OrchestrateApi.Security
{
    internal class StandardAccessAuthorizationHandler : AuthorizationHandler<StandardAccessRequirement>
    {
        private readonly IConfiguration configuration;
        private readonly IHttpContextAccessor contextAccessor;

        public StandardAccessAuthorizationHandler(IConfiguration configuration, IHttpContextAccessor contextAccessor)
        {
            this.configuration = configuration;
            this.contextAccessor = contextAccessor;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, StandardAccessRequirement requirement)
        {
            if (!context.User.HasClaim(c => c.Issuer == configuration["JWT:ValidIssuer"]))
            {
                return Task.CompletedTask;
            }

            if (context.User.IsInRole("Administrator") || context.User.IsInRole("Editor"))
            {
                context.Succeed(requirement);
            }
            else if (context.User.IsInRole("Viewer"))
            {
                if (contextAccessor.HttpContext.Request.Method == "GET")
                {
                    context.Succeed(requirement);
                }
            }

            return Task.CompletedTask;
        }
    }

    internal class StandardAccessRequirement : IAuthorizationRequirement
    {
        public const string PolicyName = "StandardAccessPolicy";
    }
}