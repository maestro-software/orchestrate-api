using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Domain.Ensembles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace OrchestrateApi.Security
{
    [Authorize(Roles = "Administrator")]
    [ApiController]
    [Route("ensemble")]
    public class EnsembleRoleController : ControllerBase
    {
        private readonly OrchestrateContext context;

        public EnsembleRoleController(OrchestrateContext context)
        {
            this.context = context;
        }

        [HttpGet("{ensembleId}/role")]
        public async Task<IEnumerable<object>> GetEnsembleRoles(int ensembleId)
        {
            return await context.EnsembleRole
                .Where(e => e.EnsembleId == ensembleId)
                .Select(e => new { Id = e.RoleId, Role = e.Role.Name })
                .ToListAsync();
        }

        [HttpPost("{ensembleId}/role/{roleId}")]
        public async Task addUserToRole(int ensembleId, string roleId)
        {
            var hasExistingEnsembleRole = await context.EnsembleRole.AnyAsync(e => e.EnsembleId == ensembleId && e.RoleId == roleId);
            if (hasExistingEnsembleRole)
            {
                return;
            }

            await context.EnsembleRole.AddAsync(new EnsembleRole
            {
                EnsembleId = ensembleId,
                RoleId = roleId,
            });
            await context.SaveChangesAsync();
        }

        [HttpDelete("{ensembleId}/role/{roleId}")]
        public async Task removeUserFromRole(int ensembleId, string roleId)
        {
            var existingEnsembleRole = await context.EnsembleRole.FirstOrDefaultAsync(e => e.EnsembleId == ensembleId && e.RoleId == roleId);
            if (existingEnsembleRole == null)
            {
                return;
            }

            context.EnsembleRole.Remove(existingEnsembleRole);
            await context.SaveChangesAsync();
        }
    }
}