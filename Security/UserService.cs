using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Domain.Members;
using OrchestrateApi.Email;

namespace OrchestrateApi.Security
{
    public class UserService
    {
        private readonly OrchestrateContext context;
        private readonly UserManager<OrchestrateUser> userManager;
        private readonly EmailService emailService;
        private readonly AppConfiguration configuration;

        public UserService(
            OrchestrateContext context,
            UserManager<OrchestrateUser> userManager,
            EmailService emailService,
            AppConfiguration configuration)
        {
            this.context = context;
            this.userManager = userManager;
            this.emailService = emailService;
            this.configuration = configuration;
        }

        public async Task UpdateUserEmail(OrchestrateUser user, string newEmail)
        {
            var token = await this.userManager.GenerateChangeEmailTokenAsync(user, newEmail);
            await this.userManager.ChangeEmailAsync(user, newEmail, token);
            await this.userManager.SetUserNameAsync(user, newEmail);
        }

        public async Task DeleteUser(string userId)
        {
            var user = await this.userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return;
            }

            await DeleteUser(user);
        }

        public async Task DeleteUser(OrchestrateUser user)
        {
            await this.userManager.DeleteAsync(user);
        }

        public async Task<JwtSecurityToken> GenerateBearerTokenAsync(OrchestrateUser user)
        {
            var userRoles = await this.userManager.GetRolesAsync(user);
            var member = await this.context.Member.FirstOrDefaultAsync(e => e.UserId == user.Id);
            var ensembleRoles = member == null
                ? (IList<string>)Array.Empty<string>()
                : await this.context.EnsembleMembership
                    .Where(i => i.MemberId == member.Id)
                    .Where(i => i.DateJoined < DateTime.UtcNow && i.DateLeft == null)
                    .Select(i => i.Ensemble.EnsembleRole)
                    .SelectMany(i => i)
                    .Select(i => i.Role.Name)
                    .ToListAsync();

            var authClaims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            foreach (var role in userRoles.Union(ensembleRoles))
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role));
            }

            var salt = configuration.JwtSecret;
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(salt));
            var token = new JwtSecurityToken(
                issuer: configuration.JwtIssuer,
                audience: configuration.FrontEndUri.ToString(),
                expires: DateTime.UtcNow.AddHours(3),
                claims: authClaims,
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));

            return token;
        }

        public async Task RequestResetPassword(string email)
        {
            var member = await GetMemberWithUserAsync(e => e.Email == email);
            if (member == null)
            {
                return;
            }

            var resetLink = await GenerateResetLinkAsync(member.User);
            await this.emailService.SendAsync(new EmailMessage
            {
                ToName = $"{member.FirstName} {member.LastName}",
                ToAddress = member.Email,
                Subject = "[Orchestrate] Password Reset Request",
                TextContent = $@"
Hi {member.FirstName},

We recently received a request to reset your password.

If this was not you, please disregard this email.

Otherwise please click this link to reset your password: {resetLink}

This link will expire in {GetTokenExpiryEstimate()}.
",
            });
        }

        public async Task InviteMember(int memberId)
        {
            var member = await GetMemberWithUserAsync(e => e.Id == memberId);
            if (member == null)
            {
                return;
            }

            var resetLink = await GenerateResetLinkAsync(member.User);
            await this.emailService.SendAsync(new EmailMessage
            {
                ToName = $"{member.FirstName} {member.LastName}",
                ToAddress = member.Email,
                Subject = $"[Orchestrate] Invitation to {this.configuration.OrganisationName}",
                TextContent = $@"
Hi {member.FirstName},

The {this.configuration.OrganisationName} has invited you to Orchestrate, a management system dedicated to community music groups.

If you are not part of this organisation, please disregard this email.

Otherwise you may set a password and log in by clicking this link: {resetLink}

This link will expire in {GetTokenExpiryEstimate()}.
",
            });
        }

        private async Task<Member> GetMemberWithUserAsync(Expression<Func<Member, bool>> memberPredicate)
        {
            var member = await this.context.Member
                .Include(e => e.User)
                .FirstOrDefaultAsync(memberPredicate);
            if (member == null)
            {
                return null;
            }

            var existingUser = await this.userManager.FindByEmailAsync(member.Email);
            if (existingUser != null && member.User == null)
            {
                member.User = existingUser;
                await this.context.SaveChangesAsync();
            }
            else if (member.User == null)
            {
                member.User = new OrchestrateUser
                {
                    SecurityStamp = Guid.NewGuid().ToString(),
                    Email = member.Email,
                    UserName = member.Email,
                };
                var result = await this.userManager.CreateAsync(member.User, Guid.NewGuid().ToString());
                if (!result.Succeeded)
                {
                    throw new InvalidOperationException("Unable to create user");
                }
            }

            return member;
        }

        private async Task<Uri> GenerateResetLinkAsync(OrchestrateUser user)
        {
            var frontendUri = this.configuration.FrontEndUri;
            var token = await this.userManager.GeneratePasswordResetTokenAsync(user);
            var tokenExpiry = DateTime.UtcNow.Add(this.configuration.TokenLifetime);
            var uriBuilder = new UriBuilder(frontendUri)
            {
                Path = "reset_password",
                Query = $"email={Uri.EscapeDataString(user.UserName)}&token={Uri.EscapeDataString(token)}&expiry={Uri.EscapeDataString(tokenExpiry.ToString("o"))}",
            };
            return uriBuilder.Uri;
        }

        private string GetTokenExpiryEstimate()
        {
            var tokenExpiry = this.configuration.TokenLifetime.TotalHours;
            return $"{tokenExpiry} hours";
        }
    }
}