using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace OrchestrateApi.Security
{
    [Authorize(Roles = "Administrator")]
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly SignInManager<OrchestrateUser> signInManager;
        private readonly UserManager<OrchestrateUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration configuration;
        private readonly OrchestrateContext context;

        public UserController(
            SignInManager<OrchestrateUser> signInManager,
            UserManager<OrchestrateUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration,
            OrchestrateContext context)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.configuration = configuration;
            this.context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<object>> GetUsers()
        {
            var query =
                from user in context.Users
                from userRole in context.UserRoles.Where(ur => ur.UserId == user.Id).DefaultIfEmpty()
                from role in context.Roles.Where(r => r.Id == userRole.RoleId).DefaultIfEmpty()
                select new
                {
                    UserId = user.Id,
                    user.UserName,
                    RoleId = role.Id,
                    RoleName = role.Name,
                };

            var results = await query.ToListAsync();
            return results.GroupBy(e => new { e.UserId, e.UserName })
                .Select(e => new
                {
                    Id = e.Key.UserId,
                    Username = e.Key.UserName,
                    Roles = e
                        .Where(e => e.RoleId != null && e.RoleName != null)
                        .Select(e => new
                        {
                            Id = e.RoleId,
                            Role = e.RoleName,
                        }),
                });
        }

        [HttpPost("{userId}/role/{roleId}")]
        public async Task addUserToRole(string userId, string roleId)
        {
            var hasExistingUserRole = await context.UserRoles.AnyAsync(e => e.UserId == userId && e.RoleId == roleId);
            if (hasExistingUserRole)
            {
                return;
            }

            await context.UserRoles.AddAsync(new IdentityUserRole<string>
            {
                UserId = userId,
                RoleId = roleId,
            });
            await context.SaveChangesAsync();
        }

        [HttpDelete("{userId}/role/{roleId}")]
        public async Task removeUserFromRole(string userId, string roleId)
        {
            var existingUserRole = await context.UserRoles.FirstOrDefaultAsync(e => e.UserId == userId && e.RoleId == roleId);
            if (existingUserRole == null)
            {
                return;
            }

            context.UserRoles.Remove(existingUserRole);
            await context.SaveChangesAsync();
        }
    }
}