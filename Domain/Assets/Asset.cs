using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.Domain.Assets
{
    public class Asset : IEntity
    {
        public Asset()
        {
            AssetLoan = new HashSet<AssetLoan>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public int Quantity { get; set; }

        public string SerialNo { get; set; }

        public DateTime? DateAcquired { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? ValuePaid { get; set; }

        public DateTime? DateDiscarded { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? ValueDiscarded { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? InsuranceValue { get; set; }

        public string CurrentLocation { get; set; }

        public string Notes { get; set; }

        public virtual ICollection<AssetLoan> AssetLoan { get; set; }
    }
}
