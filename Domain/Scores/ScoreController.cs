using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using Microsoft.AspNetCore.JsonPatch;
using OrchestrateApi.Common;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.Security;

namespace OrchestrateApi.Domain.Scores
{
    [Authorize(Policy = StandardAccessRequirement.PolicyName)]
    [ApiController]
    [Route("score")]
    public class ScoreController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly StandardEntityQueries<Score> queries;

        public ScoreController(OrchestrateContext context)
        {
            this.context = context;
            this.queries = new StandardEntityQueries<Score>(this, context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<object>>> GetScores()
        {
            return await context.Score
                .Include(e => e.PerformanceScore)
                .Select(e => new
                {
                    e.Id,
                    e.Title,
                    e.Composer,
                    e.Arranger,
                    e.Genre,
                    e.ValuePaid,
                    e.Grade,
                    e.Duration,
                    e.DatePurchased,
                    e.Location,
                    e.Notes,
                    e.IsOwned,
                    e.InLibrary,
                    PlayCount = e.PerformanceScore.Count,
                })
                .ToArrayAsync();
        }

        [HttpGet("{id}")]
        public Task<ActionResult<Score>> GetScore(int id)
        {
            return queries.Get(id);
        }

        [HttpPatch("{id}")]
        public Task<IActionResult> PutScore(int id, JsonPatchDocument<Score> scorePatch)
        {
            return queries.Patch(id, scorePatch);
        }

        [HttpPost]
        public Task<ActionResult<Score>> PostScore(Score score)
        {
            return queries.Post(score, nameof(GetScore));
        }

        [HttpDelete("{id}")]
        public Task DeleteScore(int id)
        {
            return queries.Delete(id);
        }
    }
}
