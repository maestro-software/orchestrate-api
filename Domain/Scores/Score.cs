using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Domain.Concerts;

namespace OrchestrateApi.Domain.Scores
{
    public class Score : IEntity
    {
        public Score()
        {
            PerformanceScore = new HashSet<PerformanceScore>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Composer { get; set; }

        public string Arranger { get; set; }

        public string Genre { get; set; }

        public string Grade { get; set; }

        public int? Duration { get; set; }

        public DateTime? DatePurchased { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? ValuePaid { get; set; }

        public string Location { get; set; }

        public string Notes { get; set; }

        public bool IsOwned { get; set; }

        public bool InLibrary { get; set; }

        public virtual ICollection<PerformanceScore> PerformanceScore { get; set; }
    }
}
