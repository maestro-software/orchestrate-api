using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrchestrateApi.Domain.Scores;

namespace OrchestrateApi.Domain.Concerts
{
    public class PerformanceScore
    {
        [Key]
        public int Id { get; set; }

        public int PerformanceId { get; set; }

        public int ScoreId { get; set; }

        [ForeignKey(nameof(PerformanceId))]
        public virtual Performance Performance { get; set; }

        [ForeignKey(nameof(ScoreId))]
        public virtual Score Score { get; set; }
    }
}
