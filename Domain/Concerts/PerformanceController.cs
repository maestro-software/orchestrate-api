using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace OrchestrateApi.Domain.Concerts
{
    [Authorize(Policy = StandardAccessRequirement.PolicyName)]
    [ApiController]
    [Route("performance")]
    public class PerformanceController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly StandardEntityQueries<Performance> queries;

        public PerformanceController(OrchestrateContext context)
        {
            this.context = context;
            this.queries = new StandardEntityQueries<Performance>(this, context);
        }

        [HttpGet]
        public Task<ActionResult<IEnumerable<Performance>>> GetPerformances()
        {
            return queries.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<object>> GetPerformance(int id)
        {
            var performance = await context.Performance
                .Include(e => e.Ensemble)
                .Include(e => e.Concert)
                .Include(e => e.PerformanceScores)
                    .ThenInclude(e => e.Score)
                .Include(e => e.PerformanceMembers)
                    .ThenInclude(e => e.Member)
                .Select(e => new
                {
                    e.Id,
                    e.EnsembleId,
                    e.ConcertId,
                    e.CountsAsSeperate,
                    Ensemble = new
                    {
                        e.Ensemble.Id,
                        e.Ensemble.Name,
                        e.Ensemble.IsHidden,
                    },
                    Concert = new
                    {
                        e.Concert.Id,
                        e.Concert.Date,
                        e.Concert.Occasion,
                        e.Concert.Location,
                        e.Concert.Notes
                    },
                    Score = e.PerformanceScores.Select(s => s.Score).Select(s => new
                    {
                        s.Id,
                        s.Title,
                        s.Composer,
                        s.Arranger,
                        s.Genre,
                        s.Grade,
                        s.Duration,
                        s.DatePurchased,
                        s.ValuePaid,
                        s.Location,
                        s.Notes,
                        s.IsOwned,
                        s.InLibrary,
                    }),
                    Member = e.PerformanceMembers.Select(m => m.Member).Select(m => new
                    {
                        m.Id,
                        m.FirstName,
                        m.LastName,
                    })
                })
                .FirstOrDefaultAsync(e => e.Id == id);

            if (performance == null)
            {
                return NotFound();
            }

            return performance;
        }

        [HttpPatch("{id}")]
        public Task<IActionResult> PutPerformance(int id, JsonPatchDocument<Performance> performancePatch)
        {
            return queries.Patch(id, performancePatch);
        }

        [HttpPost]
        public async Task<ActionResult<Performance>> PostPerformance(PerformanceDTO performance)
        {
            await context.Performance.AddAsync(performance);

            var performanceMembership = performance.Members.Select(memberId => new PerformanceMember
            {
                Performance = performance,
                MemberId = memberId,
            });
            await context.PerformanceMember.AddRangeAsync(performanceMembership);

            var performanceScores = performance.Scores.Select(scoreId => new PerformanceScore
            {
                Performance = performance,
                ScoreId = scoreId,
            });
            await context.PerformanceScore.AddRangeAsync(performanceScores);

            await context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPerformance), new { id = performance.Id }, null);
        }

        [HttpDelete("{id}")]
        public Task DeletePerformance(int id)
        {
            return queries.Delete(id);
        }

        [HttpPost("{performanceId}/score/{scoreId}")]
        public async Task<IActionResult> AddScoreToPerformance(int performanceId, int scoreId)
        {
            if (!await context.Performance.AnyAsync(e => e.Id == performanceId)) return NotFound();
            if (!await context.Score.AnyAsync(e => e.Id == scoreId)) return NotFound();

            var currentPerformanceScore = await context.PerformanceScore
                .FirstOrDefaultAsync(e => e.PerformanceId == performanceId && e.ScoreId == scoreId);
            if (currentPerformanceScore != null)
            {
                return Ok();
            }

            await context.PerformanceScore.AddAsync(new PerformanceScore
            {
                PerformanceId = performanceId,
                ScoreId = scoreId,
            });
            await context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{performanceId}/score/{scoreId}")]
        public async Task<IActionResult> RemoveScoreFromPerformance(int performanceId, int scoreId)
        {
            var performanceScore = await context.PerformanceScore
                .FirstOrDefaultAsync(e => e.PerformanceId == performanceId && e.ScoreId == scoreId);
            if (performanceScore != null)
            {
                context.PerformanceScore.Remove(performanceScore);
                await context.SaveChangesAsync();
            }

            return Ok();
        }

        [HttpPost("{performanceId}/member/{memberId}")]
        public async Task<IActionResult> AddMemberToPerformance(int performanceId, int memberId)
        {
            if (!await context.Performance.AnyAsync(e => e.Id == performanceId)) return NotFound();
            if (!await context.Member.AnyAsync(e => e.Id == memberId)) return NotFound();

            var currentPerformanceMember = await context.PerformanceMember
                .FirstOrDefaultAsync(e => e.PerformanceId == performanceId && e.MemberId == memberId);
            if (currentPerformanceMember != null)
            {
                return Ok();
            }

            await context.PerformanceMember.AddAsync(new PerformanceMember
            {
                PerformanceId = performanceId,
                MemberId = memberId,
            });
            await context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{performanceId}/member/{memberId}")]
        public async Task<IActionResult> RemoveMemberFromPerformance(int performanceId, int memberId)
        {
            var performanceMember = await context.PerformanceMember
                .FirstOrDefaultAsync(e => e.PerformanceId == performanceId && e.MemberId == memberId);
            if (performanceMember != null)
            {
                context.PerformanceMember.Remove(performanceMember);
                await context.SaveChangesAsync();
            }

            return Ok();
        }

        public class PerformanceDTO : Performance
        {
            public int[] Members { get; set; }

            public int[] Scores { get; set; }
        }
    }
}