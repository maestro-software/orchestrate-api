using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OrchestrateApi.Domain.Concerts
{
    public class Concert : IEntity
    {
        public Concert()
        {
            Performance = new HashSet<Performance>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Occasion { get; set; }

        public string Location { get; set; }

        public DateTime? Date { get; set; }

        public string Notes { get; set; }

        public virtual ICollection<Performance> Performance { get; set; }
    }
}
