using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace OrchestrateApi.Domain.Ensembles
{
    public partial class EnsembleRole
    {
        [Key]
        public int Id { get; set; }

        public int EnsembleId { get; set; }

        public string RoleId { get; set; }

        [ForeignKey(nameof(EnsembleId))]
        public virtual Ensemble Ensemble { get; set; }

        [ForeignKey(nameof(RoleId))]
        public virtual IdentityRole Role { get; set; }
    }
}
