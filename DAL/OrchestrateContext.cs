using OrchestrateApi.Domain.Assets;
using OrchestrateApi.Domain.Concerts;
using OrchestrateApi.Domain.Ensembles;
using OrchestrateApi.Domain.Members;
using OrchestrateApi.Domain.Scores;
using OrchestrateApi.Security;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;

namespace OrchestrateApi.DAL
{
    public partial class OrchestrateContext : IdentityDbContext<OrchestrateUser>
    {
        public OrchestrateContext()
        {
        }

        public OrchestrateContext(DbContextOptions<OrchestrateContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Asset> Asset { get; set; }
        public virtual DbSet<AssetLoan> AssetLoan { get; set; }
        public virtual DbSet<Concert> Concert { get; set; }
        public virtual DbSet<Ensemble> Ensemble { get; set; }
        public virtual DbSet<EnsembleMembership> EnsembleMembership { get; set; }
        public virtual DbSet<EnsembleRole> EnsembleRole { get; set; }
        public virtual DbSet<Member> Member { get; set; }
        public virtual DbSet<MemberInstrument> MemberInstrument { get; set; }
        public virtual DbSet<Performance> Performance { get; set; }
        public virtual DbSet<PerformanceMember> PerformanceMember { get; set; }
        public virtual DbSet<PerformanceScore> PerformanceScore { get; set; }
        public virtual DbSet<Score> Score { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Ensemble>()
                .HasIndex(i => i.Name)
                .IsUnique();

            modelBuilder.Entity<Member>(entity =>
            {
                entity.HasIndex(i => i.Email)
                    .IsUnique();
                entity.Property(i => i.FeeClass)
                    .HasDefaultValue("Standard");
                entity.Property(e => e.MembershipClass)
                    .HasDefaultValue("Ordinary");
            });

            modelBuilder.Entity<PerformanceMember>()
                .HasIndex(e => new { e.MemberId, e.PerformanceId })
                .IsUnique();

            modelBuilder.Entity<PerformanceScore>()
                .HasIndex(e => new { e.PerformanceId, e.ScoreId })
                .IsUnique();

            OnModelCreatingPartial(modelBuilder);

            // Use SnakeCase for database naming. Adapted from:
            // https://andrewlock.net/customising-asp-net-core-identity-ef-core-naming-conventions-for-postgresql/#replacing-the-default-conventions-with-snake-case
            // Is an alternative to https://github.com/efcore/EFCore.NamingConventions which doesn't seem to 
            // handle EF Identity Core and also is a dependency I'd rather not rely on
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                // Replace table names
                entity.SetTableName(entity.GetTableName().ToSnakeCase());

                // Replace column names            
                foreach (var property in entity.GetProperties())
                {
                    property.SetColumnName(property.GetColumnName().ToSnakeCase());
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(key.GetConstraintName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetName(index.GetName().ToSnakeCase());
                }
            }
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
