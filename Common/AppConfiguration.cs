using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace OrchestrateApi.Common
{
    public class AppConfiguration
    {
        public AppConfiguration(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public string MailjetPublicKey => GetAppConfig("MailjetPublicKey");

        public string MailjetSecretKey => GetAppConfig("MailjetSecretKey");

        public string DefaultFromEmail => GetAppConfig("DefaultFromEmail");

        public string OrganisationName => GetAppConfig("OrganisationName");

        public Uri FrontEndUri => new Uri(GetAppConfig("FrontEndUri"));

        public string ToEmailOverride => GetAppConfig("ToEmailOverride");

        public TimeSpan TokenLifetime => TimeSpan.FromHours(GetAppConfig<int>("TokenLifetimeHours"));

        public string JwtIssuer => Configuration["JWT:ValidIssuer"];

        public string JwtSecret => Configuration["JWT:Secret"];

        public string ConnectionString => ConvertUrlStringToConnectionString(Configuration["DATABASE_URL"]);

        private string ConvertUrlStringToConnectionString(string databaseUrl)
        {
            // Adapted from https://gist.github.com/snow-jallen/bbcef9a7c50056e227e066010c043a4c#file-startup-cs
            // Hopefully will be able to remove this if https://github.com/npgsql/npgsql/pull/2733 gets merged
            var uri = Uri.TryCreate(databaseUrl, UriKind.Absolute, out var parsedUri)
                ? parsedUri
                : throw new InvalidOperationException("Invalid database uri");

            var connectionStringParts = new List<string>
            {
                $"Host={uri.Host}",
                "SSL Mode=Prefer",
                "Trust Server Certificate=true",
            };

            if (uri.Port > 0)
            {
                connectionStringParts.Add($"Port={uri.Port}");
            }

            var database = uri.Segments.LastOrDefault();
            if (database != null)
            {
                connectionStringParts.Add($"Database={database}");
            }

            if (!string.IsNullOrEmpty(uri.UserInfo))
            {
                var userParts = uri.UserInfo.Split(':');
                connectionStringParts.Add($"Username={userParts.First()}");

                if (userParts.Length > 1)
                {
                    connectionStringParts.Add($"Password={userParts.Last()}");
                }
            }

            return string.Join("; ", connectionStringParts);
        }

        private string GetAppConfig(string property)
        {
            return Configuration[$"AppConfiguration:{property}"];
        }

        private T GetAppConfig<T>(string property)
        {
            return Configuration.GetValue<T>($"AppConfiguration:{property}");
        }
    }
}