using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OrchestrateApi.DAL;

namespace OrchestrateApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                .Build()
                .MigrateDatabase()
                .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://*:5000");
                    webBuilder.UseSentry();
                });
    }

    internal static class ProgramExtensions
    {
        // TODO Don't really want to do it this way if we end up with multiple hosts,
        // but for velocity's sake, this is quick and easy.
        public static IHost MigrateDatabase(this IHost host)
        {
            using var scope = host.Services.CreateScope();

            var serviceProvider = scope.ServiceProvider;

            var environment = serviceProvider.GetRequiredService<IWebHostEnvironment>();
            if (environment.IsDevelopment())
            {
                return host;
            }

            try
            {
                var context = serviceProvider.GetRequiredService<OrchestrateContext>();
                context.Database.Migrate();
            }
            catch (Exception e)
            {
                var logger = serviceProvider.GetRequiredService<ILogger<Program>>();
                logger.LogError(e, "Failed to migrate database");
            }

            return host;
        }
    }
}
