using System;
using System.Text;
using System.Threading.Tasks;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using OrchestrateApi.Email;

namespace OrchestrateApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            // Can't inject AppConfiguration since it hasn't been registered yet
            Configuration = new AppConfiguration(configuration);
        }

        public AppConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<OrchestrateContext>(opt =>
            {
                opt.UseNpgsql(Configuration.ConnectionString);
            });

            services.AddIdentity<OrchestrateUser, IdentityRole>(options =>
                {
                    // TODO setup default users and configure this to realistic values
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 1;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                })
                .AddEntityFrameworkStores<OrchestrateContext>()
                .AddDefaultTokenProviders();
            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = Configuration.TokenLifetime;
            });
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false; // TODO true if not dev
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = Configuration.FrontEndUri.ToString(),
                    ValidIssuer = Configuration.JwtIssuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.JwtSecret))
                };
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy(StandardAccessRequirement.PolicyName, policy =>
                {
                    policy.Requirements.Add(new StandardAccessRequirement());
                });
            });

            services.AddCors(opt =>
            {
                opt.AddDefaultPolicy(policy =>
                {
                    // Must remove trailing '/' that Uri adds
                    var uri = Configuration.FrontEndUri.ToString();
                    var origin = uri.Remove(uri.Length - 1);

                    policy.WithOrigins(origin)
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });
            services.AddControllers()
                // We only add Newtonsoft to support camelCase in patch updates
                // (and so we don't have to add the Content-Type: application/json-patch+json header)
                // TODO Look at rolling our own later?
                .AddNewtonsoftJson();

            services.AddHttpClient();

            services.AddHttpContextAccessor();
            services.AddSingleton<IAuthorizationHandler, StandardAccessAuthorizationHandler>();
            services.AddTransient<AppConfiguration>();
            services.AddTransient<EmailService>();
            services.AddTransient<UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (!env.IsDevelopment())
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();
            app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
